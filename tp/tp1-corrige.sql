-- -----------------------------------------------------------------------
-- Partie accès aux données
-- -----------------------------------------------------------------------

-- on crée le serveur "distant", ici vers la base 'pedago' on lève ici les
-- droits d'écritures (que les utilisateurs n'ont pas de toute façon)
CREATE SERVER IF NOT EXISTS ro_local_bd_pedago
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (
    host 'bd-pedago.univ-lyon1.fr', 
    port '5432', 
    dbname 'pedago',  
    updatable 'false');

-- pour annuler
-- DROP SERVER IF EXISTS ro_local_bd_pedago;

--  on dit que l'utilisateur local REMPLACEZ_MOI_PAR_VOTRE_LOGIN accède
--  au distant via le même compte REMPLACEZ_MOI_PAR_VOTRE_LOGIN :
--  il faut préciser le mot de passe
CREATE USER MAPPING IF NOT EXISTS FOR REMPLACEZ_MOI_PAR_VOTRE_LOGIN 
SERVER ro_local_bd_pedago 
OPTIONS (
    user 'REMPLACEZ_MOI_PAR_VOTRE_LOGIN', 
    password 'REMPLACEZ_MOI_PAR_VOTRE_MOT_DE_PASSE_POSTGRES');

-- pour annuler
-- DROP USER MAPPING IF EXISTS FOR REMPLACEZ_MOI_PAR_VOTRE_LOGIN SERVER ro_local_bd_pedago;

-- enfin pour ajouter, ici UNE TABLE DU SCHEMA json
IMPORT FOREIGN SCHEMA json
  LIMIT TO (pods)
  FROM SERVER ro_local_bd_pedago INTO public;

-- on teste
SELECT COUNT(*)
FROM pods;
-- résultat: 216

-- -----------------------------------------------------------------------
-- Partie 2 requêtes JSON Path
-- -----------------------------------------------------------------------

-- ------------------------------------
-- 2.1
-- ------------------------------------

-- Test de requête
select jsonb_path_query(data, '$.metadata.uid') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- resultat: "45c71a9d-55aa-482f-9fb2-b12931b272d9"
-- remarquer les "": le résultat est au format jsonb

-- Requête $.metadata
select jsonb_path_query(data, '$.metadata') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';

-- ------------------------------------
-- 2.2
-- ------------------------------------

-- 1. Quel est le nom (_name_) du pod (dans les métadonnées) ?
select jsonb_path_query(data, '$.metadata.name') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- résultat
-- "prometheus-adhoc-vm-prometheus-0"


-- 2. Quels sont les différents types de conditions par lesquelles ce pod est
--    passé (dans _status_) ?
select jsonb_path_query(data, '$.status.conditions[*].type') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- 4 résultats: 
-- Initialized
-- Ready
-- ContainersReady
-- PodScheduled


-- 3. Donner le nom (_name_) des _volumes_ (dans _spec_) qui possèdent un champ _secret_
select jsonb_path_query(data, '$.spec.volumes[*] ?(exists(@.secret)) .name') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- deux résultats:
-- "config"
-- "web-config"


-- 4. Donner les arguments (_args_) du conteneur (_containers_ dans _spec_) nommé "prometheus".
select jsonb_path_query(data, '$.spec.containers[*] ?(@.name == "prometheus") .args[*]') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- 8 résultats
-- "--web.console.templates=/etc/prometheus/consoles"
-- "--web.console.libraries=/etc/prometheus/console_libraries"
-- "--storage.tsdb.retention.time=1y"
-- "--config.file=/etc/prometheus/config_out/prometheus.env.yaml"
-- "--storage.tsdb.path=/prometheus"
-- "--web.enable-lifecycle"
-- "--web.route-prefix=/"
-- "--web.config.file=/etc/prometheus/web_config/web-config.yaml"


-- 5. Donner les types de conditions (dans _status_) dont le _lastTransitionTime_ est plus grand 
--    que l'heure de démarrage (_startedAt_) d'un des conteneurs (accessible dans _containerStatuses_).
select jsonb_path_query(data, '$.status.conditions[*] ?(@.lastTransitionTime > $.status.containerStatuses.state.*.startedAt) .type') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- 2 résultats:
-- "Ready"
-- "ContainersReady"
 

-- 6. Donner la liste des clés du dictionnaire _labels_ dans _metadata_.
--    On pourra commencer par donner l'ensemble des clés/valeurs.
select jsonb_path_query(data, '$.metadata.labels.keyvalue().key') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- 9 résultats
-- prometheus
-- app.kubernetes.io/name
-- controller-revision-hash
-- app.kubernetes.io/version
-- app.kubernetes.io/instance
-- operator.prometheus.io/name
-- app.kubernetes.io/managed-by
-- operator.prometheus.io/shard
-- statefulset.kubernetes.io/pod-name

-- 7. Donner la liste des différent états (clé dans _state_) des conteneurs 
--    (_containerStatuses_ et _initContainerStatuses_).
select jsonb_path_query(data, '$.status.*[*].state.keyvalue().key') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
-- 3 résultats
-- "running"
-- "running"
-- "terminated"

-- -----------------------------------------------------------------------
-- Partie 3 JSON Path dans SQL
-- -----------------------------------------------------------------------


-- 8. Donner l'ensemble des différents labels (les clés, pas les valeurs) 
--    utilisés dans l'ensemble des pods.
select jsonb_path_query(data, '$.metadata.labels.keyvalue().key') as pod_label
from pods;
-- 1107 résultats

-- 9. Transformer la requête précédente pour renvoyer le texte qui est le 
--    seul constituant du JSON retourné à la question 8. En profiter pour 
--    éliminer les doublons.
select distinct jsonb_path_query(data, '$.metadata.labels.keyvalue().key') #>> '{}' as pod_label
from pods;
-- 68 résultats


-- 10. On souhaite calculer le nombre de paires clé/valeur distinctes que l'on
--     peut trouver dans les _labels_ des pods. Pour cela, on pourra utiliser une
--     relation intermédiaire qui extrait les paires clés/valeurs (i.e. un e
--     variante de la question 8.) dans un résultat json. On extraira ensuite
--     dans la requête principale la clé et la valeur via `->>`, et on en
--     comptera les valeurs distinctes. Il est conseillé de procéder étape par
--     étape pour faire cette requête (mettre d'abord au point la requête
--     intermédiaire, puis faire une requête principale qui extrait séparément la
--     clé et la valeur et enfin la requête finale).

with kv as 
(select jsonb_path_query(data, '$.metadata.labels.keyvalue()') as keyvals
from pods)
select count(distinct (keyvals ->> 'key', keyvals ->> 'value')) as cnt
from kv;
-- 429 résultats


-- 11. Créer une vue `containers(name, image, pod_name, pod_id)`
--     ([doc](https://www.postgresql.org/docs/current/sql-createview.html))
--     permettant d'obtenir des informations sur les _containers_ et les
--     _initContainers_ définits dans _spec_ (attention, les informations sur les
--     pods sont dans _metadata_). Il est conseillé de mettre au point la requête
--     en la limitant à deux pods dans un premier temps, de façon à pouvoir
--     vérifier les résultats.
create or replace view containers as
with 
pod_infos as
(select
jsonb_path_query(data, '$.metadata.name') #>> '{}' as pod_name,
uid as pod_id,
data
from pods), 
containers_infos as 
(select 
	jsonb_path_query(data, '$.spec.keyvalue() ?(@.key == "containers" || @.key == "initContainers"). value[*]') as container,
	pod_name,
	pod_id
from pod_infos)
select 
	container ->> 'name' as name,
	container ->> 'image' as image,
	pod_name,
	pod_id
from containers_infos
;

-- 12. Compter le nombre de conteneurs, le résultat doit être 431.
select count(*) from containers;


-- 13. Écrire une requête qui génère un objet json de la forme `{ "nom": "a",
--     "image": "b" }` pour chaque tuple dans `containers` où `name` vaut _a_ et
--     `image` vaut _b_.
select jsonb_build_object('nom', name, 'image', image) as j
from containers;

-- 14. Écrire une requête permettant d'obtenir un document de la forme suivante pour chaque image:
--
-- ```
-- { "image": "nom_image",
--   "containers": [
--     { "nom": "container1", "pod": "nom_du_pod1" },
--     { "nom": "container2", "pod": "nom_du_pod2" },
--   ]
-- }
-- ```

select 
	image, -- pas demandé, mais pratique pour vérifier
	jsonb_build_object(
		'image', image,
		'containers', jsonb_agg(jsonb_build_object(
			'nom', name,
			'pod', pod_name
		)) 				
	) as j
from containers
group by image; 




