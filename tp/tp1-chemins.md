# TP JSON Path et PostgreSQL

**Objectifs:** Dans ce TP, on pratiquera dans un premier temps les requêtes JSON
Path sur des données JSON stockées dans une base de données PostgreSQL. Dans un
deuxième temps on verra comment extraire des données JSON pour les placer dans
des attributs relationnels classiques.

Documentation:

- [Accès à la base PostgreSQL](https://forge.univ-lyon1.fr/bd-pedago/bd-pedago)
- [PostgreSQL: fonctions json](https://www.postgresql.org/docs/current/functions-json.html)
- [PostgreSQL: type json](https://www.postgresql.org/docs/current/datatype-json.html)

## 1. Accès aux données

Les données ont été placées dans la base de données `pedago` dans le schema
`json`. Afin d'accéder à ces données on va créer un lien (un _foreign data
wrapper_) depuis sa base personnelle, vers la base pedago, puis importer la
définition de la table `pods`.

Le code SQL suivant permet de faire ces opérations. Attention à bien remplacer
**toutes** les occurences de `REMPLACEZ_MOI_PAR_VOTRE_LOGIN` et de
`REMPLACEZ_MOI_PAR_VOTRE_MOT_DE_PASSE_POSTGRES` pour que cela fonctionne.

```sql
-- on crée le serveur "distant", ici vers la base 'pedago'
-- on lève ici les droits d'écritures (que les utilisateurs n'ont pas de toute façon)
CREATE SERVER IF NOT EXISTS ro_local_bd_pedago
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (host 'bd-pedago.univ-lyon1.fr', port '5432', dbname 'pedago',  updatable 'false');

-- pour annuler
-- DROP SERVER IF EXISTS ro_local_bd_pedago;

--  on dit que l'utilisateur local REMPLACEZ_MOI_PAR_VOTRE_LOGIN accède
--  au distant via le même compte REMPLACEZ_MOI_PAR_VOTRE_LOGIN :
--  il faut préciser le mot de passe
CREATE USER MAPPING IF NOT EXISTS FOR REMPLACEZ_MOI_PAR_VOTRE_LOGIN SERVER ro_local_bd_pedago OPTIONS (user 'REMPLACEZ_MOI_PAR_VOTRE_LOGIN', password 'REMPLACEZ_MOI_PAR_VOTRE_MOT_DE_PASSE_POSTGRES');

-- pour annuler
-- DROP USER MAPPING IF EXISTS FOR REMPLACEZ_MOI_PAR_VOTRE_LOGIN SERVER ro_local_bd_pedago;

-- enfin pour ajouter, ici UNE TABLE DU SCHEMA json
IMPORT FOREIGN SCHEMA json
  LIMIT TO (pods)
  FROM SERVER ro_local_bd_pedago INTO public;

-- on teste
SELECT COUNT(*)
FROM pods;
-- résultat: 216

```

Un exemple de document disponible dans la table `pods`:
[pod-sample.json](tp1-ressources/pod-sample.json).

## 2. Requêtes JSON Path

### 2.1 Exécution des requêtes

Dans la partie 2, on va exécuter quelques requêtes JSON Path sur un document
particulier stocké dans la table `pods`. Pour cela (dans cette partie
uniquement) on executera la requête suivante, en remplaçant
`MA_REQUETE_JSONPATH` par la requête que l'on souhaite exécuter:

```sql
select jsonb_path_query(data, 'MA_REQUETE_JSONPATH') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
```

Par exemple, pour lancer la requête `$.metadata.uid`, on exécutera:

```sql
select jsonb_path_query(data, '$.metadata.uid') as resultat
from pods
where uid = '45c71a9d-55aa-482f-9fb2-b12931b272d9';
```

> Si on exécute la requête `$.metadata`, qu'obtient-on ?
> Expliquer pourquoi dans le résultat de la requête `$.metadata.uid` on a des `"`.

**Remarque:** dans DBeaver, lorsque les résultats sont présentés, un onglet
"Visionneuse de valeurs" apparait sur la droite. En cliquant sur l'icône
figurant un livre juste à droite de l'onglet, on peut sélectionner l'option
"Autoformat" qui simplifiera la lecture des valeurs.

### 2.2 Interrogation des informations du pod 45c71a9d-55aa-482f-9fb2-b12931b272d9

Si ce n'est pas déjà fait, regarder le document
[pod-sample.json](tp1-ressources/pod-sample.json). C'est ce document qui est
interrogé dans les requêtes de la section 2.1.

Pour chacune des demandes suivantes, donner une requête JSON Path pour y répondre en interrogeant les données JSON du document `45c71a9d-55aa-482f-9fb2-b12931b272d9`

> 1. Quel est le nom (_name_) du pod (dans les métadonnées) ?
>    (1 réponse)

> 2. Quels sont les différents types de conditions par lesquelles ce pod est passé (dans _status_)?
>    (4 réponses)

> 3. Donner le nom (_name_) des _volumes_ (dans _spec_) qui possèdent un champ _secret_
>    (2 réponses)

> 4. Donner les arguments (_args_) du conteneur (_containers_ dans _spec_) nommé "prometheus".
>    (8 réponses)

> 5. Donner les types de conditions (dans _status_) dont le _lastTransitionTime_ est plus grand que l'heure de démarrage (_startedAt_) d'un des conteneurs (accessible dans _containerStatuses_).
>    (2 réponses)

Le JSON Path de PostgreSQL permet l'utilisation de fonctions pour manipuler les
valeurs dans une expression JSON Path (typiquement dans des conditions).
Consulter la [table des opérateurs et méthodes
jsonpath](https://www.postgresql.org/docs/current/functions-json.html#FUNCTIONS-SQLJSON-OP-TABLE), en particulier l'opérateur `keyvalue()` (à la fin).

> 6. Donner la liste des clés du dictionnaire _labels_ dans _metadata_. On pourra commencer par donner l'ensemble des clés/valeurs.
>    (9 réponses)

> 7. Donner la liste des différent états (clé dans _state_) des conteneurs (_containerStatuses_ et _initContainerStatuses_).
>    (3 réponses)

## 3. Utilisation de JSON Path dans un autre langage: SQL

JSON Path est souvent utilisé en conjonction avec d'autres langages, par exemple des langages de programmation. Ici nous allons en voir l'utilisation dans le langage de requête SQL.

Comme on a pu le constater précédement, il est possible d'utiliser la fonction `jsonb_path_query` pour exécuter une requête JSON Path sur une donnée JSON dans une requête SQL. On peut remarquer que les requêtes précédentes interrogent une seule ligne de la table `pods`, cependant certaines requêtes renvoient plusieurs lignes résultat. La fonction `jsonb_path_query` peut donc produire plusieurs lignes de résultat par ligne du `FROM` (à l'opposé d'un `GROUP BY` qui lui va au contraire rassembler plusieurs lignes du `FROM`en une seule ligne résultat).

> 8. Donner l'ensemble des différents labels (les clés, pas les valeurs)
>    utilisés dans l'ensemble des pods.
>    (1107 réponses)

On peut constater l'existence de doublons dans les résultats. Cependant, ajouter
un `DISTINCT` dans la requête ne résout pas le problème. C'est parce que le
type retourné par `jsonb_path_query` est `jsonb` et que PostgreSQL ne fait pas
de comparaison sur ce type. Il faut donc le transformer en texte.
Pour cela on peut utiliser l'opérateur d'extraction `#>>` (_c.f._ [doc](https://www.postgresql.org/docs/14/functions-json.html#FUNCTIONS-JSON-OP-TABLE)) avec un chemin vide (noté `'{}'`).

> 9. Transformer la requête précédente pour renvoyer le texte qui est le seul
>    constituant du JSON retourné à la question 8. En profiter pour éliminer les
>    doublons.
>    (68 réponses)

En PostgreSQL, l'opérateur `->>` permet d'accéder à un champ ou à une case dans un contenu JSON. Comme indiqué dans la documentation le résultat est de type `text` (i.e. c'est du texte pour SQL et pas du JSON contenant du texte).

On rappelle par ailleurs la construction `WITH` de SQL ([doc](https://www.postgresql.org/docs/current/queries-with.html)) avec l'exemple ci dessous:

```sql
WITH ma_relation_intermediaire AS
(SELECT a, count(b) as nb_b
FROM toto
GROUP BY a)
SELECT count(a) as nb_a
FROM ma_relation_intermediaire
WHERE nb_b > 3
```

Cette construction permet de simplifier l'écriture des requête lorsque l'on souhaite utiliser des sous-requêtes dans le `FROM`

On rappelle également que pour compter les paires (a,b) distinctes, on peut utiliser `count(distinct (a,b))`.

> 10. On souhaite calculer le nombre de paires clé/valeur distinctes que l'on
>     peut trouver dans les _labels_ des pods. Pour cela, on pourra utiliser une
>     relation intermédiaire qui extrait les paires clés/valeurs (i.e. un e
>     variante de la question 8.) dans un résultat json. On extraira ensuite
>     dans la requête principale la clé et la valeur via `->>`, et on en
>     comptera les valeurs distinctes. Il est conseillé de procéder étape par
>     étape pour faire cette requête (mettre d'abord au point la requête
>     intermédiaire, puis faire une requête principale qui extrait séparément la
>     clé et la valeur et enfin la requête finale).
>     (429 réponses)

> 11. Créer une vue `containers(name, image, pod_name, pod_id)`
>     ([doc](https://www.postgresql.org/docs/current/sql-createview.html))
>     permettant d'obtenir des informations sur les _containers_ et les
>     _initContainers_ définits dans _spec_ (attention, les informations sur les
>     pods sont dans _metadata_). Il est conseillé de mettre au point la requête
>     en la limitant à deux pods dans un premier temps, de façon à pouvoir
>     vérifier les résultats.

> 12. Compter le nombre de conteneurs, le résultat doit être 431.

## 4. Génération de JSON avec SQL

Dans cette dernière partie, on prendra en main les fonctions de création de
contenu JSON, puis on les utilisera de façon à faire, _in fine_, une
transformation de contenu JSON.

La fonction `jsonb_build_object`
([doc](https://www.postgresql.org/docs/current/functions-json.html#FUNCTIONS-JSON-CREATION-TABLE))
permet de créer un objet JSON en fonction du nom et de la valeur des champs.

> 13. Écrire une requête qui génère un objet json de la forme `{ "nom": "a", "image": "b" }` pour chaque tuple dans `containers` où `name` vaut _a_ et
>     `image` vaut _b_.
>     (431 résultats)

La fonction d'aggrégation `jsonb_agg`
([doc](https://www.postgresql.org/docs/current/functions-aggregate.html)) permet
de fabriquer un tableau json à partir d'un ensemble de valeurs json.

> 14. Écrire une requête permettant d'obtenir un document de la forme suivante pour chaque image :
>
> ```json
> {
>   "image": "nom_image",
>   "containers": [
>     { "nom": "container1", "pod": "nom_du_pod1" },
>     { "nom": "container2", "pod": "nom_du_pod2" }
>   ]
> }
> ```
>
>     (107 résultats)

Remarque: il sera nécessaire d'utiliser `jsonb_agg` en argument de `jsonb_build_object`, mais aussi de faire un autre appel à `jsonb_build_object` en argument de `jsonb_agg`.

À titre indicatif, l'image `busybox:latest` est référencée dans 3 containers.
